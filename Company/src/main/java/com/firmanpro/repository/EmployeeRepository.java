package com.firmanpro.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.firmanpro.model.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long>{

}
