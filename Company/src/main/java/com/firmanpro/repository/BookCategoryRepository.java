package com.firmanpro.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.firmanpro.model.one2many.BookCategory;

public interface BookCategoryRepository extends JpaRepository<BookCategory, Long>{

}
