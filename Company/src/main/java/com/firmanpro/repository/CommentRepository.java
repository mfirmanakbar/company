package com.firmanpro.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.firmanpro.model.Comment;

public interface CommentRepository extends JpaRepository<Comment, Long>{

}
