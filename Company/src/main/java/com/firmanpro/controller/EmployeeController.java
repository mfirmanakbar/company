package com.firmanpro.controller;

import java.util.HashMap;
import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.firmanpro.dao.EmployeeDAO;
import com.firmanpro.model.Employee;
import com.firmanpro.model.one2many.Book;
import com.firmanpro.model.one2many.BookCategory;
import com.firmanpro.repository.BookCategoryRepository;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.Set;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

	@Autowired
	EmployeeDAO employeeDAO;

	/* to save an employee */
	@PostMapping("/create")
	public Employee createEmployee(@Valid @RequestBody Employee emp) {
		return employeeDAO.save(emp);
	}

	/* get all employees */
	@GetMapping("/getall")
	public List<Employee> getAllEmployees() {
		return employeeDAO.findAll();
	}

	/* get all employees */
	@GetMapping("getalls")
	public HashMap<String, Object> get() {
		HashMap<String, Object> map = new HashMap<>();
		map.put("ket", "semua data employee");
		map.put("results", employeeDAO.findAll());
		return map;
	}

	/* get employee by empid */
	@GetMapping("/getone/{id}")
	public ResponseEntity<Employee> getEmployeeById(@PathVariable(value = "id") Long empid) {

		Employee emp = employeeDAO.findOne(empid);
		if (emp == null) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok().body(emp);

	}

	/* update an employee by empid */
	@PutMapping("/update/{id}")
	public ResponseEntity<Employee> updateEmployee(@PathVariable(value = "id") Long empid,
			@Valid @RequestBody Employee empDetails) {

		Employee emp = employeeDAO.findOne(empid);
		if (emp == null) {
			return ResponseEntity.notFound().build();
		}

		emp.setName(empDetails.getName());
		emp.setDesignation(empDetails.getDesignation());
		emp.setExpertise(empDetails.getExpertise());

		Employee updateEmployee = employeeDAO.save(emp);
		return ResponseEntity.ok().body(updateEmployee);

	}

	/* Delete an employee */
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Employee> deleteEmployee(@PathVariable(value = "id") Long empid) {

		Employee emp = employeeDAO.findOne(empid);
		if (emp == null) {
			return ResponseEntity.notFound().build();
		}
		employeeDAO.delete(emp);

		return ResponseEntity.ok().build();

	}

	// begin - one to many test
	// https://hellokoding.com/jpa-one-to-many-relationship-mapping-example-with-spring-boot-maven-and-mysql/
	@Autowired
	private BookCategoryRepository bookCategoryRepository;

	@GetMapping("/onetomanysave")
	@Transactional
	public String onetomanySave() {
		BookCategory categoryA = new BookCategory("Category A");
		Set bookAs = new HashSet<Book>() {
			{
				add(new Book("Book A1", categoryA));
				add(new Book("Book A2", categoryA));
				add(new Book("Book A3", categoryA));
			}
		};
		categoryA.setBooks(bookAs);

		BookCategory categoryB = new BookCategory("Category B");
		Set bookBs = new HashSet<Book>() {
			{
				add(new Book("Book B1", categoryB));
				add(new Book("Book B2", categoryB));
				add(new Book("Book B3", categoryB));
			}
		};
		categoryB.setBooks(bookBs);

		bookCategoryRepository.save(new HashSet<BookCategory>() {
			{
				add(categoryA);
				add(categoryB);
			}
		});
		return "add successfuly!";
	}
	
	@GetMapping("/onetomanygetall")
	@Transactional
	public String onetomanyGetAll() {
		// fetch all categories
		for (BookCategory bookCategory : bookCategoryRepository.findAll()) {
			// logger.info(bookCategory.toString());
			System.out.println("load data - begin");
			System.out.println(bookCategory.toString());
			System.out.println("load data - ending");
		}
		return "get all successfuly!"; 
	}
		
	// end - one to many test

}
